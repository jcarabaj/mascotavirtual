package Archivo;

import Mascota.modelo.Alimento;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import mascotavirtual.VentanaLogin;

/**
 *
 * @author jacr
 */
public class Archivo {
    
    public static String filename="alimentos.txt";
    
    public void CrearArchivo(String usuario, String pass) throws NullPointerException, IOException{
        
        File file= new File("MascotaVitual.txt");
        try(BufferedWriter escribe= new BufferedWriter(new FileWriter(file, true))){
            
            String escri = usuario+","+pass;
            escribe.write(escri);
            escribe.newLine();
            
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
     }  
    
     public static ArrayList<Alimento> leerAlimentaciones() throws IOException{
       
        ArrayList<Alimento> alimentos=new ArrayList<>();;
        try(BufferedReader bufer= new BufferedReader(new FileReader(filename))) {
                String linea=" ";
                while((linea=bufer.readLine())!=null){
                    String [] separador=linea.split(",");
                    alimentos.add(new Alimento(separador[0],Integer.parseInt(separador[1]), 
                                                     separador[2],Integer.parseInt(separador[3])));
                }
            } catch (IOException e) {
                System.out.println("Fin del archivo");
         }

        return alimentos;
    }
     public static ArrayList<String>LeerArchivo() throws FileNotFoundException, IOException{
         ArrayList<String> usuarios= new ArrayList<>();
         try(BufferedReader leer= new BufferedReader(new FileReader("MascotaVitual.txt"))){
             String linea1=" ";
             while((linea1=leer.readLine())!=null){
                 String [] separador=linea1.split(",");
                // usuarios.add()
             }
         }
         return usuarios;
     }
}
