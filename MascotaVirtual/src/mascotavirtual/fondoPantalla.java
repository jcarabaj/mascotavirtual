/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author CltControl
 */
public class fondoPantalla {
    public ImageView fondoPortada, fondoLogin, fondoGalaxia, fondoGame;
    
    public final String RUTA1="/fondo/escenarioLimpio3.jpg";
    public final String RUTA2="/fondo/escenaLimpia2.jpg";
    public final String RUTA3="/fondo/scenaLimpia.jpg";
    public final String RUTA4="/fondo/escenaJuego.jpg";
    

    public fondoPantalla() {
        fondoPortada= new ImageView(new Image(RUTA1));
        fondoPortada.setFitHeight(Constantes.TAM_X);
        fondoPortada.setFitWidth(Constantes.TAM_Y);
        fondoPortada.setLayoutX(0);
        fondoPortada.setLayoutY(0);
        
        fondoLogin= new ImageView(new Image(RUTA2));
        fondoLogin.setFitHeight(Constantes.TAM_X);
        fondoLogin.setFitWidth(Constantes.TAM_Y);
        fondoLogin.setLayoutX(0);
        fondoLogin.setLayoutY(0);
        
        fondoGalaxia= new ImageView(new Image(RUTA3));
        fondoGalaxia.setFitHeight(Constantes.TAM_X);
        fondoGalaxia.setFitWidth(Constantes.TAM_Y);
        fondoGalaxia.setLayoutX(0);
        fondoGalaxia.setLayoutY(0);
        
        fondoGame= new ImageView(new Image(RUTA4));
        fondoGame.setFitHeight(Constantes.TAM_X);
        fondoGame.setFitWidth(Constantes.TAM_Y);
        fondoGame.setLayoutX(0);
        fondoGame.setLayoutY(0);
    }

    public ImageView getFondoPortada() {
        return fondoPortada;
    }

    public ImageView getFondoLogin() {
        return fondoLogin;
    }

    public ImageView getFondoGalaxia() {
        return fondoGalaxia;
    }

    public ImageView getFondoGame() {
        return fondoGame;
    }

    
    
    
    
}
