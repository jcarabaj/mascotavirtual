/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import java.util.LinkedList;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 *
 * @author CltControl
 */
public class Basura {
    private  ImageView basura;
    
    private  String rutaBasura= "/Mascota/Pelota.png";
    

    public Basura() {
        
      basura=new ImageView(new Image(rutaBasura));
      basura.setFitHeight(60);
      basura.setFitWidth(60);
        
        
    }

    public ImageView getBasura() {
        return basura;
    }

    public void setBasura(ImageView basura) {
        this.basura = basura;
    }

    public String getRutaBasura() {
        return rutaBasura;
    }

    public void setRutaBasura(String rutaBasura) {
        this.rutaBasura = rutaBasura;
    }
    
    
}
    
    