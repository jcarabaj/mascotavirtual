
package mascotavirtual;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author jacr
 */
public class ImaAlimento {
    public ImageView alimento1, alimento2, alimento3, alimento4,alimento5,alimento6,alimento7,alimento8;
    public final String RUTA1="/Comida/Pizza.png";
    public final String RUTA2="/Comida/Lasagna.png";
    public final String RUTA3="/Comida/dona1.png";
    public final String RUTA4="Comida/agua.png";
    public final String RUTA5="Comida/broccoli1.png";
    public final String RUTA6="Comida/cupcake.png";
    public final String RUTA7="Comida/Helado.png";
    public final String RUTA8="Comida/jugo1.png";
    
    ImaAlimento(){
        alimento1= new ImageView(new Image(RUTA1));
        alimento1.setFitHeight(70);
        alimento1.setFitWidth(70);
        alimento1.setLayoutX(50);
        alimento1.setLayoutY(50);
        
        alimento2= new ImageView(new Image(RUTA2));
        alimento2.setFitHeight(70);
        alimento2.setFitWidth(70);
        alimento2.setLayoutX(50);
        alimento2.setLayoutY(100);
        
        alimento3= new ImageView(new Image(RUTA3));
        alimento3.setFitHeight(70);
        alimento3.setFitWidth(70);
        alimento3.setLayoutX(50);
        alimento3.setLayoutY(150);
        
        
        alimento4= new ImageView(new Image(RUTA4));
        alimento4.setFitHeight(70);
        alimento4.setFitWidth(70);
        alimento4.setLayoutX(50);
        alimento4.setLayoutY(200);
        
        alimento5= new ImageView(new Image(RUTA5));
        alimento5.setFitHeight(70);
        alimento5.setFitWidth(70);
        alimento5.setLayoutX(50);
        alimento5.setLayoutY(200);
        
        alimento6= new ImageView(new Image(RUTA6));
        alimento6.setFitHeight(70);
        alimento6.setFitWidth(70);
        alimento6.setLayoutX(50);
        alimento6.setLayoutY(200);
        
        alimento7= new ImageView(new Image(RUTA7));
        alimento7.setFitHeight(70);
        alimento7.setFitWidth(70);
        alimento7.setLayoutX(50);
        alimento7.setLayoutY(200);
        
        alimento8= new ImageView(new Image(RUTA8));
        alimento8.setFitHeight(70);
        alimento8.setFitWidth(70);
        alimento8.setLayoutX(50);
        alimento8.setLayoutY(200);
    }
    public ImageView getAlimento1(){
        return alimento1;
    }
    public ImageView getAlimento2(){
        return alimento2;
    }
    public ImageView getAlimento3(){
        return alimento3;
    }
    public ImageView getAlimento4(){
        return alimento4;
    }

    public ImageView getAlimento5() {
        return alimento5;
    }

    public ImageView getAlimento6() {
        return alimento6;
    }

    public ImageView getAlimento7() {
        return alimento7;
    }

    public ImageView getAlimento8() {
        return alimento8;
    }
    
}
