package mascotavirtual;

import Archivo.Archivo;
import Mascota.modelo.Alimento;
import Mascota.modelo.Mascota;
import Tiempo.Reloj;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

/**
 *
 * @author jacr
 */
public class VentanaAccion {
    public BorderPane root1;
    public Pane panel;
    public VBox vbox,vbEstados,VboxCom;
    public Button b2,b3,b4;
    public ImaMascota mascota;
    private Reloj reloj;
    private StackPane root,hora,estadoInf;
    private ImageView estadoHora;
    public final String RUTA5="/fondo/estados.png";
    public final String RUTA6="/fondo/estados1.png";
    public MascotaVirtual masVirtual;
    static Basura b;
    Timeline tiempo;
    LinkedList <Basura> LBasura ;
    VentanaAccion() throws IOException{
        LBasura =new LinkedList<Basura>();
        masVirtual= new MascotaVirtual();
        root1= new BorderPane();
        hora=new StackPane();
        estadoInf=new StackPane();
        vbEstados= new VBox();
        vbox= new VBox();
        VboxCom= new VBox();
        panel= new Pane();
        root=new StackPane();
        estadoHora=new ImageView(new Image(RUTA5));
        estadoHora.setFitHeight(90);
        estadoHora.setFitWidth(300);
        manejarTiempoBasura();
        
        reloj=new Reloj();
        mascota= new ImaMascota();
        //b1= new Button("Alimentar");
        b4=new Button();
        b4.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView("icon_button/atras.png");
	image1.setFitWidth(100);
	image1.setFitHeight(100);
	b4.setGraphic(image1);
        b4.setOnAction(e-> {
            try {
                CambiarVentana(e);
            } catch (IOException ex) {
                Logger.getLogger(VentanaAccion.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
            for(Alimento al: Archivo.leerAlimentaciones()){
                 FlowPane comida= new FlowPane();
            
                    ImageView imagen= new ImageView(al.getRutaImagen());
                    imagen.setFitHeight(40);
                    imagen.setFitWidth(40);
                    Button nombre= new Button(al.getNombre());
                    Label precio= new Label(" "+al.getPrecio());
                    comida.getChildren().addAll(precio,nombre,imagen);
                    VboxCom.getChildren().add(comida);
                    
                    root1.setBottom(VboxCom);
                    nombre.setOnAction(e->{DescontarDinero( al, masVirtual.getMascota().getDinero());
                                        AumentarAlimentacion(al, masVirtual.getMascota().getAlimentacion());
                            });
                   }
        b2= new Button();
        b2.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("icon_button/limpiar.png");
	image.setFitWidth(50);
	image.setFitHeight(50);
	b2.setGraphic(image);
        b2.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
               for (Basura i : LBasura){
                    panel.getChildren().remove(i.getBasura());
               }
              // MascotaVirtual.mascota.setLimpieza(10);
               masVirtual.getMascota().setLimpieza(10);
                }
        });
        b3= new Button();
        b3.setStyle("-fx-background-color: transparent; ");
        ImageView imag = new ImageView("icon_button/jugar.png");
	imag.setFitWidth(90);
	imag.setFitHeight(70);
	b3.setGraphic(imag);
        
        
        b3.setOnAction(e-> {
            try {
                CambiarVentana(e);
            } catch (IOException ex) {
                Logger.getLogger(VentanaAccion.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        panel.getChildren().add(mascota.getMascota1());
        vbox.getChildren().addAll(b2,b3,VboxCom);
        
        
        vbox.setSpacing(20);
        hora.getChildren().addAll(estadoHora,reloj.getLblReloj());
        vbEstados.getChildren().addAll(reloj.getEdad(),
        reloj.getLblAnimo(),reloj.getLblAlimentacion()
       , reloj.getLblDinero());
        
        root1.setTop(hora);
        root1.setRight(vbox);
        root1.setLeft(b4);
        root1.setCenter(panel);
        root1.setBottom(vbEstados);
        Animacion(mascota.getMascota1(), new Ellipse(60,60));
        
       
        root.getChildren().addAll((new fondoPantalla().getFondoGalaxia()),root1);
    }
    public void CambiarVentana(ActionEvent e) throws IOException{

        if(e.getSource().equals(b4))
            MascotaVirtual.Vmascota();
        if(e.getSource().equals(b2))
            MascotaVirtual.Vmascota();
        if(e.getSource().equals(b3))
            MascotaVirtual.VentanaGame();
    }
    
    
    public void DescontarDinero( Alimento al, int saldo){
       int precio=al.getPrecio();
     reloj.getLblDinero().setText("Dinero: "+(saldo-precio));
       masVirtual.getMascota().setDinero(saldo-precio);
       System.out.println("esta comiendo: "+al.getNombre()+"  este aliemnto cuesta: "+al.getPrecio());
   }
   public void AumentarAlimentacion(Alimento al, int alimentacion){
       int alimento= al.getQuitaHambre();
       reloj.getLblAlimentacion().setText("Alimentacion: "+(alimentacion+alimento));
       masVirtual.getMascota().setAlimentacion(alimentacion+alimento);
       System.out.println("este alimento aumenta: "+al.getQuitaHambre());
   }
   
     void Animacion(Node n,Shape s){
         //1.- Creo un objeto tipo path trasition
         PathTransition transicion = new PathTransition();
         //2.- Seteo el nodo que quiero que se mueva
         transicion.setNode(n);
         //3.- Selecciono la forma en que el nodo se va a mover
         transicion.setPath(s);
         //4.-Desde donde se va ubicar la animacion
         s.setLayoutX(100);
         s.setLayoutY(100);
         //La duracion de la transicion
         transicion.setDuration(Duration.seconds(10));
         //Cuantas veces se va a repetir la animacion
         transicion.setCycleCount(Timeline.INDEFINITE);
         transicion.autoReverseProperty();
         //Ejecutar la animacion
         transicion.play();
    }
     
     void manejarTiempoBasura(){
        Random r = new Random();
        int num = r.nextInt((int) 5);
        Timeline verificar = new Timeline();
        tiempo = new Timeline();
        KeyFrame kfsegundo = new KeyFrame(Duration.seconds(num),e->crearBasura());
        //KeyFrame kfsegundo1 = new KeyFrame(Duration.seconds(1),e->verificarReloj());
        tiempo.getKeyFrames().addAll(kfsegundo);
        tiempo.setCycleCount(Timeline.INDEFINITE);
        tiempo.play();
        //verificar.getKeyFrames().addAll(kfsegundo1);
        verificar.setCycleCount(Timeline.INDEFINITE);
        verificar.play();
    }
     
     public void crearBasura(){        
        b = new Basura();
        root.getChildren().add(b.getBasura());
        Random r = new Random();
        int num = r.nextInt((int) 100);
        b.getBasura().setLayoutX(num);
        Random a = new Random();
        int num1 = a.nextInt((int) 100);
        b.getBasura().setLayoutY(num1);
        LBasura.add(b);
        //le resto 1 a la limpieza cada vez que se crea una basura en la pantalla.
        MascotaVirtual.mascota.setLimpieza(MascotaVirtual.mascota.getLimpieza()-1);
        panel.getChildren().addAll(b.getBasura());
    }

    public StackPane getRoot() {
        return root;
    }

    public VBox getVbEstados() {
        return vbEstados;
    }
    
    
    
}
