package mascotavirtual;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author jacr
 */
public class ImaMascota {
    public  ImageView mascota1;
    public final String RUTA1="/mascota/buzz.png";
    //public final String RUTA2="/mascota/mascota2.jpg";
    ImaMascota(){
        mascota1= new ImageView(new Image(RUTA1));
        mascota1.setFitHeight(180);
        mascota1.setFitWidth(180);
        mascota1.setLayoutX(200);
        mascota1.setLayoutY(200);
        
        /*mascota2= new ImageView(new Image(RUTA2));
        mascota2.setFitHeight(60);
        mascota2.setFitWidth(60);
        mascota2.setLayoutX(400);
        mascota2.setLayoutY(400);*/
        
    }

    public ImageView getMascota1() {
        return mascota1;
    }
    
    /*public ImageView getMascota2() {
        return mascota2;
    }*/
    
}
