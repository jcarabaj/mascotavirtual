/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import javafx.animation.PathTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 *
 * @author CltControl
 */
public class BolasEnergia {

    private ImageView imagenBolasEnergia;
    private static final String ARCHIVO_IMAGEN = "/Mascota/bolaEnergia.png";
    private Line line;

    BolasEnergia() {
        inicializarElementos();

    }

    /**
     * TODO: Inicialice los elementos, linea e image view, basandose en
     * ciudadano
     */
    public void inicializarElementos() {
        imagenBolasEnergia = new ImageView(new Image(ARCHIVO_IMAGEN));
        line = new Line();
        imagenBolasEnergia.setFitHeight(40);
        imagenBolasEnergia.setFitWidth(40);

    }

    
    PathTransition animacion(double x) {
        //1.- Creo un objeto tipo path trasition
        PathTransition transicion = new PathTransition();
        //2.- Seteo el nodo que quiero que se mueva
        transicion.setNode(imagenBolasEnergia);
        //3.- Selecciono la forma en que el nodo se va a mover
        transicion.setPath(parametrizarLinea(x));
        //La duracion de la transicion
        transicion.setDuration(Duration.seconds(5));
        //Cuantas veces se va a repetir la animacion
        transicion.setCycleCount(1);
        //Ejecutar la animacion
        transicion.play();

        return transicion;
    }

    public Line parametrizarLinea(double x) {
        System.out.println(x);

        line.setStartX(x);
        line.setEndX(x);
        line.setEndY(0);
        line.setStartY(700 - 150);
        return line;
    }

    /**
     * TODO: Cree getters necesarios
     */
    public ImageView getImagenBolasEnergia() {
        return imagenBolasEnergia;
    }

    public Line getLine() {
        return line;
    }

}
