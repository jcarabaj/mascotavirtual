package mascotavirtual;
import java.io.IOException;
import Mascota.modelo.Mascota;
import java.time.LocalDateTime;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

    public class MascotaVirtual extends Application {
        public static Scene scene;
         public static  Mascota mascota =new Mascota();
    @Override
    public void start(Stage stage) throws Exception {
        
        
        VentanaPortada va= new VentanaPortada();
        scene= new Scene(va.getRoot(),Constantes.TAM_Y,Constantes.TAM_X);
        stage.setScene(scene);
        stage.setTitle("Mascota Virtual");
         scene.onKeyPressedProperty().bind(va.getRoot().onKeyPressedProperty());
        stage.show();
    }
      public static void main(String[] args) {
        launch(args);
    }
      public static void VentanaPortada(){
          VentanaPortada va= new VentanaPortada();
          scene.setRoot(va.getRoot());
      }
      
      public static void VentanaLogin(){
        VentanaLogin vl= new VentanaLogin();
          scene.setRoot(vl.getRoot());
      }
       public Mascota getMascota() {
        return mascota;
    }
      

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }
      public static void Vmascota(){
          Pane root = new Pane();
          Vmascota vm= new Vmascota();
          scene.setRoot(vm.getRoot());
      }
      public static void VentanaAccion() throws IOException{
          VentanaAccion va= new VentanaAccion();
          scene.setRoot(va.getRoot());
      }
      
      public static void VentanaGame(){
          VentanaGame va= new VentanaGame();
          scene.setRoot(va.getRoot());
          //scene.onKeyPressedProperty().bind(va.getRoot().onKeyPressedProperty());
      }
      
      
}
