/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import Mascota.modelo.Mascota;
import java.util.Random;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 *
 * @author CltControl
 */
public class VentanaGame {

    private StackPane root;
    private Pane paneJuego;
    Mascota mascota= MascotaVirtual.mascota;
    public VentanaGame() {

        root = new StackPane();
        paneJuego = new Pane();
        root.getChildren().addAll((new fondoPantalla().getFondoGame()), paneJuego, (new Movimiento().getJuego()));
        creaNuevoMeteorito();
        Thread ejecutor = new Thread(new Ejecutor());
        ejecutor.start();
        
    }
    

    void creaNuevoMeteorito() {
        Random r = new Random();
        int num = r.nextInt(2);
        String cadena = "";
        cadena = String.valueOf(num);
        Meteorito m = new Meteorito(cadena);

        paneJuego.getChildren().addAll(m.getPane());

        m.getPane().setOnMouseClicked(e -> remueveDePanel(e));
        //Animacion(new ImaMascota().getMascota1(), new Ellipse(60,60));
        if(num==0){
            m.getPane().setOnMouseClicked(e->clickAlien());
            m.getPane().setOnMouseClicked(a-> remuevePane(a));
        }if(num==1){
            m.getPane().setOnMouseClicked(e->clickAlien());
            m.getPane().setOnMouseClicked(a-> remuevePane(a));
        }
    }
    
    public void clickAlien(){
        System.out.println("Bomba");
        
    }
    public void remuevePane(MouseEvent a){
        paneJuego.getChildren().remove(a.getSource());
    }
public void moverObjeto(KeyEvent e) {

        if (e.getCode().equals(KeyCode.UP)) {
            System.out.println("a");
            mascota.getImagenMascota().setLayoutX(mascota.getImagenMascota().getLayoutX() + 5);
        } else if (e.getCode().equals(KeyCode.DOWN)) {
            mascota.getImagenMascota().setLayoutX(mascota.getImagenMascota().getLayoutX() - 5);
        }  
}
    public StackPane getRoot() {
        return root;
    }

    void remueveDePanel(MouseEvent e) {
        paneJuego.getChildren().remove(e.getSource());
    }

    public Pane getPaneJuego() {
        return paneJuego;
    }

    public class Ejecutor implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                Platform.runLater(() -> creaNuevoMeteorito());

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    System.out.println("error");
                }
            }
        }
    }

    

}
