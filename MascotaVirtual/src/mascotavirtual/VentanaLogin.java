package mascotavirtual;

import Archivo.Archivo;
import Tiempo.Reloj;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author jacr
 */
public class VentanaLogin {
    public BorderPane root1;
    public VBox Vlogin;
    public HBox hbox1, hbox2,hbox3;
    public Button crear,b1;
    private StackPane root;
    public TextField user,pass;
    
    
    public VentanaLogin(){
        Archivo archivo= new Archivo();
        Vlogin= new VBox();
        hbox1= new HBox();
        hbox2= new HBox();
        hbox3= new HBox();
        
        root1=new BorderPane();
        
        
        Label lb1= new Label("Nombre de Usuario");
        lb1.setFont(Font.font(30));
        lb1.setTextFill(Color.RED);
        Label lb2= new Label("Nombre de la Mascota");
        lb2.setFont(Font.font(30));
        lb2.setTextFill(Color.RED);
         user= new TextField();
         pass= new TextField();
        
        crear= new Button();
        crear.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("icon_button/crearCuenta.png");
	image.setFitWidth(200);
	image.setFitHeight(100);
	crear.setGraphic(image);
        crear.setOnAction(e->{ 
            VentanaMascota(e);
        try{    
            System.out.println(user.getText());
            System.out.println(pass.getText());
            archivo.CrearArchivo(user.getText(),pass.getText());  
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
         
        });
        crear.setFont(Font.font(20));
        //crear.setStyle();
        b1= new Button();
        b1.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView("icon_button/atras.png");
	image1.setFitWidth(100);
	image1.setFitHeight(100);
	b1.setGraphic(image1);
        b1.setOnAction(e-> VentanaMascota(e));
        
        hbox1.getChildren().addAll(lb1,user);
        hbox1.setSpacing(10);
        hbox1.setAlignment(Pos.CENTER);
        
        hbox2.getChildren().addAll(lb2,pass);
        hbox2.setSpacing(10);
        hbox2.setAlignment(Pos.CENTER);
       
        hbox3.getChildren().addAll(crear);
        hbox3.setSpacing(30);
        hbox3.setAlignment(Pos.CENTER);
       
        Vlogin.setAlignment(Pos.CENTER);
        Vlogin.setSpacing(20);
        Vlogin.getChildren().addAll(hbox1,hbox2,hbox3);
        root1.setCenter(Vlogin);
        root1.setLeft(b1);
        
        
        root=new StackPane();
        root.getChildren().addAll((new fondoPantalla().getFondoLogin()),root1);
        
        
    }
    public void VentanaMascota(ActionEvent e){
        if(e.getSource().equals(crear))
            MascotaVirtual.Vmascota();
        if(e.getSource().equals(b1))
            MascotaVirtual.VentanaPortada();
    }

    public StackPane getRoot() {
        return root;
    }
    public TextField getUser() {
        return user;
    }
    public TextField getPass() {
        return pass;
    }
   
}
