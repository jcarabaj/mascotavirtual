
package mascotavirtual;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author CltControl
 */
public class VentanaPortada {
    private Button b1,b2;
    private VBox vbx;
    private Label lbl1;
    private StackPane root;

    public VentanaPortada() {
  
        b1=new Button();
        b1.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("icon_button/boton-inicio.png");
	image.setFitWidth(200);
	image.setFitHeight(80);
	b1.setGraphic(image);
        b1.setFont(Font.font(20));
        
        b2=new Button();
        b2.setFont(Font.font(20));
        b2.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView("icon_button/salir.png");
	image1.setFitWidth(100);
	image1.setFitHeight(100);
	b2.setGraphic(image1);
        
        lbl1=new Label("Ha salido del juego");
        root=new StackPane();
        vbx=new VBox();
        vbx.getChildren().addAll(b1,b2);
        vbx.setAlignment(Pos.BOTTOM_CENTER);
        vbx.setSpacing(15);
        root.getChildren().addAll((new fondoPantalla()).getFondoPortada(),vbx);
        b1.setOnAction(e-> buttonAccion(e));
        b2.setOnAction(e-> buttonAccion(e));
        
        
       
    }

    public StackPane getRoot() {
        return root;
    }

    
    
    
    
    
    public void buttonAccion(ActionEvent e){
        if(e.getSource().equals(b1))
            MascotaVirtual.VentanaLogin();
        if(e.getSource().equals(b2)){
            System.out.println(lbl1);
            Platform.exit();
        }
    }
    
    
}
