package mascotavirtual;

import Tiempo.Reloj;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author jacr
 */
public class Vmascota {
    public BorderPane root1;
    public ImaMascota mascota;
    public VBox vbox1;
    public StackPane root;
    
    public Button b1,b2;
    public Vmascota(){
        root1= new BorderPane();
        vbox1= new VBox();
        
        
        b1= new Button();
        b1.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("icon_button/ok.png");
	image.setFitWidth(100);
	image.setFitHeight(100);
	b1.setGraphic(image);
        b1.setOnAction(e-> {
            try {
                CambiarPantalla(e);
            } catch (IOException ex) {
                Logger.getLogger(Vmascota.class.getName()).log(Level.SEVERE, null, ex);
            }
        });//accion para cambiar de Pantalla
        b1.setFont(Font.font(20));
        b2= new Button();
        b2.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView("icon_button/atras.png");
	image1.setFitWidth(100);
	image1.setFitHeight(100);
	b2.setGraphic(image1);
        b2.setOnAction(e-> {
            try {
                CambiarPantalla(e);
            } catch (IOException ex) {
                Logger.getLogger(Vmascota.class.getName()).log(Level.SEVERE, null, ex);
            }
        });//accion para cambiar de Pantalla
        mascota= new ImaMascota();
        Label lb1= new Label("Su Mascota ha sido creada");
        lb1.setAlignment(Pos.CENTER);
        lb1.setFont(Font.font(50));
        lb1.setTextFill(Color.YELLOW);
        vbox1.getChildren().addAll(mascota.getMascota1(),b1);
        vbox1.setAlignment(Pos.CENTER);
        vbox1.setSpacing(10);
        
        root=new StackPane();
        
       
        root1.setTop(lb1);
        root1.setCenter(vbox1);
        root1.setLeft(b2);
        
        root.getChildren().addAll((new fondoPantalla().getFondoGalaxia()),root1);
       // Reloj r=new Reloj();
    }
    public void CambiarPantalla(ActionEvent e) throws IOException{
        if(e.getSource().equals(b1))
            MascotaVirtual.VentanaAccion();
        if(e.getSource().equals(b2))
            MascotaVirtual.VentanaLogin();
        
    }

    public StackPane getRoot() {
        return root;
    }

    
    
}
