/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import java.util.Random;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 *
 * @author ALEXANDRA
 */
public class Meteorito {
    private ImageView imagenWieb;
    private static final String RUTA1 = "/Mascota/Alien.png";
    private StackPane pane;
    private Text text;

    Meteorito(String texto) {
        pane = new StackPane();
        text = new Text(texto);
        text.setFill(Color.WHITE);
        imagenWieb = new ImageView(new Image(RUTA1));
        imagenWieb.setFitHeight(50);
        imagenWieb.setFitWidth(50);

        agregarPane();
        animacion();
    }

    public void agregarPane() {
        pane.getChildren().addAll(imagenWieb, text);
    }

    public void animacion() {
        PathTransition transition = new PathTransition();
        
        transition.setNode(pane);
        
        Random r = new Random();
        int num = r.nextInt(700);
        Line linea = new Line(num, 0, num, 700);
        transition.setPath(linea);

        transition.setDuration(Duration.seconds(10));

        transition.setCycleCount(Timeline.INDEFINITE);

        
        transition.setAutoReverse(false);
        
        transition.play();

    }

    public StackPane getPane() {
        return pane;
    }
}
