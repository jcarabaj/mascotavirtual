/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotavirtual;

import Mascota.modelo.Mascota;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author CltControl
 */
public class Movimiento {

    private static Pane juego;
    
    private Mascota mascota;
    

    public Movimiento() {
        inicializarControles();
        ubicarElementos();
    }

    void inicializarControles() {
        juego = new Pane();
        mascota = new Mascota();

        

        agregarPane();
        manejarEvento();

    }

    void agregarPane() {
        juego.getChildren().addAll(mascota.getImagenMascota());
        //juego.getChildren().addAll(rect1,rect2,rect3);
    }

    
    public void manejarEvento() {
        //juego.setOnKeyPressed(e -> moverObjeto(e));
        juego.setOnMouseClicked(e -> lanzarBolas(e));

    }

    
    public void lanzarBolas(MouseEvent e) {
        BolasEnergia bolas = new BolasEnergia();
        juego.getChildren().add(bolas.getImagenBolasEnergia());
        System.out.println("Buzz" + mascota.getImagenMascota().getLayoutX());
        bolas.animacion(mascota.getImagenMascota().getLayoutX() + 50).setOnFinished(ev -> {
            juego.getChildren().remove(bolas.getImagenBolasEnergia());
        });

    }

    
//    public void moverObjeto(KeyEvent e) {
//
//        if (e.getCode().equals(KeyCode.UP)) {
//            System.out.println("a");
//            mascota.getImagenMascota().setLayoutX(mascota.getImagenMascota().getLayoutX() + 5);
//        } else if (e.getCode().equals(KeyCode.DOWN)) {
//            mascota.getImagenMascota().setLayoutX(mascota.getImagenMascota().getLayoutX() - 5);
//        }
 //   }

    void ubicarElementos() {
        mascota.getImagenMascota().setLayoutX(200);
        mascota.getImagenMascota().setLayoutY(700 - 200);
    }

    public Pane getJuego() {
        return juego;
    }

}
