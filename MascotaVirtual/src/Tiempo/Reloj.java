/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tiempo;

import Mascota.modelo.Mascota;
import java.time.LocalDateTime;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import mascotavirtual.MascotaVirtual;




/**
 *
 * @author ALEXANDRA
 */
public class Reloj {
    private Label lblReloj,lbledad;
    private Label lblAlimentacion, lblAnimo, lblDinero;//Status
        Timeline RelojAlimento ;
        Timeline RelojVida;
        Timeline RelojAnimo; 
        public MascotaVirtual masVirtual;
       //public  Mascota mascota;
        public int cont=0;
    public Reloj(){
        lblReloj= new Label(LocalDateTime.now().toString());
        //mascota= new Mascota();
        masVirtual=new MascotaVirtual();
       lbledad= new Label(String.valueOf(masVirtual.getMascota().getAnioVida()));
       lbledad.setFont(Font.font(20));
       lbledad.setTextFill(Color.RED);
       //
       lblAlimentacion= new Label("Alimentacion: "+masVirtual.getMascota().getAlimentacion());
       lblAlimentacion.setFont(Font.font(20));
       lblAlimentacion.setTextFill(Color.RED);
       lblAnimo = new Label();
       lblAnimo.setFont(Font.font(20));
       lblAnimo.setTextFill(Color.RED);
       lblDinero= new Label("Dinero: "+masVirtual.getMascota().getDinero());
       lblDinero.setFont(Font.font(20));
       lblDinero.setTextFill(Color.RED);
        manejarTiempo();
        
    }
    
   
    
    void manejarTiempo(){
         RelojAlimento = new Timeline();
         RelojVida = new Timeline();
         RelojAnimo= new Timeline();
         
        KeyFrame kfsegundo = new KeyFrame(Duration.seconds(1),e->{
            manejaSegundo();
                });
        KeyFrame kfminuto = new KeyFrame(Duration.seconds(3),e->{
            controlaVida();
                });
        KeyFrame kfAnimo = new KeyFrame(Duration.seconds(20),e->{
            manejaAnimo();
                });
        RelojAlimento.getKeyFrames().addAll(kfsegundo);
        RelojVida.getKeyFrames().addAll(kfminuto);
        RelojAnimo.getKeyFrames().add(kfAnimo);
        
        RelojAlimento.setCycleCount(Timeline.INDEFINITE);
        RelojVida.setCycleCount(Timeline.INDEFINITE);
        RelojAnimo.setCycleCount(Timeline.INDEFINITE);
        
        RelojAlimento.play();
        RelojVida.play();
        RelojAnimo.play();
    }
    void controlaVida(){
        //System.out.println("Ha pasado un minuto mas en tu vida");
        System.out.println("su mascota tiene: "+masVirtual.getMascota().getAnioVida()+" anio de vida");
        masVirtual.getMascota().setAlimentacion(masVirtual.getMascota().getAlimentacion()-1);
       lblAlimentacion.setText("Alimentacion: "+masVirtual.getMascota().getAlimentacion());
        masVirtual.getMascota().setAnioVida(masVirtual.getMascota().getAnioVida()+1);
        lbledad.setText("su mascota tiene: "+masVirtual.getMascota().getAnioVida()+" año de vida");
        if(cont==masVirtual.getMascota().getAnioMaxVida()){   
            System.out.println("su mascota ha muerto por años de edad");
            Platform.exit();
        }
        if (masVirtual.getMascota().getAlimentacion()==0){
            System.out.println("su mascota ha muerto por falta de alimento");
            Platform.exit();
        }
         cont++;
        }
    void manejaAnimo(){
        System.out.println("su animo es: "+masVirtual.getMascota().getAnimo());
        masVirtual.getMascota().setAnimo(masVirtual.getMascota().getAnimo()-1);
        lblAnimo.setText("el animo es: "+masVirtual.getMascota().getAnimo());
       if(masVirtual.getMascota().getAnimo()==0){
           System.out.println("su mascota ha muerto por falta de animo");
           Platform.exit();
           
       }
    }
    void manejaSegundo(){
        lblReloj.setText(LocalDateTime.now().toString());
        long horaActual = System.currentTimeMillis();
        
    }

    public Label getLblReloj() {
        return lblReloj;
    }

    public Label getEdad() {
        return lbledad;
    }
    public Label getLblAnimo() {
        return lblAnimo;
    }

    public Label getLblAlimentacion() {
        return lblAlimentacion;
    }

    public Label getLblDinero() {
        return lblDinero;
    }
       
}
    
    

