package Mascota.modelo;

/**
 *
 * @Jacr
 */
public class Alimento {
    private String nombre;
    private int precio;
    private String rutaImagen;
    private int quitaHambre;

    public Alimento(String nombre, int precio, String rutaImagen,int quitaHambre) {
        this.nombre = nombre;
        this.precio = precio;
        this.rutaImagen = rutaImagen;
        this.quitaHambre= quitaHambre;
    }

    @Override
    public String toString() {
        return "Alimento{" + "nombre=" + nombre + ", precio=" + precio + 
                ", rutaImagen=" + rutaImagen + ", quita hambre"+quitaHambre+'}';
    }

    public int getQuitaHambre() {
        return quitaHambre;
    }

    public void setQuitaHambre(int quitaHambre) {
        this.quitaHambre = quitaHambre;
    }
    
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }
    
}
