package Mascota.modelo;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author jacr
 */
public class Mascota {

    private int alimentacion = 10;
    private int animo = 10;
    private int dinero = 100;
    private int limpieza = 10;
     private int anioVida=0;
    private int anioMaxVida=100;
   
    private ImageView imagenMascota;
    private static final String ARCHIVO_IMAGEN = "/Mascota/buzz.png";

    public Mascota(int alimentacion,int animo,int dinero, int limpieza,int anioVida, int anioMaxVida){
        this.alimentacion=alimentacion;
        this.animo= animo;
        this.dinero= dinero;
        this.limpieza= limpieza;
         this.anioVida= this.anioVida;
        this.anioMaxVida= anioMaxVida;
    }
    
    public Mascota() {
        alimentacion = 10;
        animo = 10;
        dinero = 100;
        limpieza = 10;
        
       // inicializarElementos();

    }

    public int getAnioVida() {
        return anioVida;
    }

    public void setAnioVida(int anioVida) {
        this.anioVida = anioVida;
    }

    public void setLimpieza(int limpieza) {
        this.limpieza = limpieza;
    }

    public void setImagenMascota(ImageView imagenMascota) {
        this.imagenMascota = imagenMascota;
    }
    

    public int getAnioMaxVida() {
        return anioMaxVida;
    }

    public void setAnioMaxVida(int anioMaxVida) {
        this.anioMaxVida = anioMaxVida;
    }

    public int getAlimentacion() {
        return alimentacion;
    }

    public int getAnimo() {
        return animo;
    }

    public int getDinero() {
        return dinero;
    }

    public int getLimpieza() {
        return limpieza;
    }
    
    public static String getARCHIVO_IMAGEN() {
        return ARCHIVO_IMAGEN;
    }

    
    
    public static void alimentar() {

    }

    public static void animar() {
    }

    public static void limpiar() {
    }

    public static void comprar() {
    }

//    public void inicializarElementos() {
//        imagenMascota = new ImageView(new Image(ARCHIVO_IMAGEN));
//        imagenMascota.setFitHeight(190);
//        imagenMascota.setFitWidth(220);
//    }

    public ImageView getImagenMascota() {
        return imagenMascota;
    }

    public void setAlimentacion(int alimentacion) {
        this.alimentacion = alimentacion;
    }

    public void setAnimo(int animo) {
        this.animo = animo;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }

}
